*** Settings ***
Library     ConserverLibrary

*** Test Cases ***

Open Close
    [Setup]         Open Console  ttyUSB5
    [Teardown]      Close Console

    No Operation


Write
    [Setup]         Open Console  ttyUSB5
    [Teardown]      Close Console

    Write Data      show tr069 conf\n


Read With Terminator
    [Setup]         Open Console  ttyUSB5
    [Teardown]      Close Console

    Write Data      show tr069 conf\n
    ${read} =       Read Until  terminator=>>
    Should Contain  ${read}  Command executed

    Write Data      show foo conf\n
    ${read} =       Read Until  terminator=>>
    Should Contain  ${read}  Wrong argument


    Write Data      mow\n
    ${read} =       Read Until  terminator=>>
    Should Contain  ${read}  not found


Read With Size
    [Setup]         Open Console  ttyUSB5
    [Teardown]      Close Console

    Write Data      show tr069 conf\n
    ${read} =       Read Until  size=10000
    Should Contain  ${read}  Command executed

    Write Data      show foo conf\n
    ${read} =       Read Until  terminator=>>
    Should Contain  ${read}  Wrong argument


    Write Data      mow\n
    ${read} =       Read Until  size=1000
    Should Contain  ${read}  not found


