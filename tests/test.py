from ConserverLibrary import *
from time import sleep
import traceback

cs = ConserverLibrary(encoding="utf-8")

print(cs.list_consoles())

cs.open_console("ttyUSB2", "-Mconsole.aethra.intra")
# reset
cs.write_data("\x12")
# login
cs.write_data("foo\n")
cs.write_data("\n")
# send commands
cs.write_data("show tr069 conf\n")
cs.write_data("show foo conf\n")
cs.write_data("show tr069 conf\n")

sleep(1)

while True:
    r = cs.read_until(terminator=">>")
    if not r:
        break
    print(">>>>>>>>>>>>>>>>>>>>>>>>>")
    print(r)

cs.close_console()
