import subprocess
import logging
import fcntl
import os
import signal
from threading import Thread
from collections import deque
import time
import re

from robot.api import logger

threadbuffer = deque()
#threadbuffer = io.StringIO()

LF = '\n'

def async_reader(proc, buffer):
    assert isinstance(buffer, deque)

    while proc and proc.poll() is None:
        # need to read one character at the time, otherwise it may block waiting for more characters
        # and not returning the piece of data that is available (and that, incidentally, may
        # contain the terminator used by read_until)

        try:
            chunk = proc.stdout.read(1)
            buffer.append(chunk)    
        except Exception as e:  
            logger.warn("Error reading from console: %s" % e)
            break


class ConserverLibrary:

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'
    ROBOT_LIBRARY_VERSION = (0,1,0)


    def __init__(self, timeout=1, encoding="ascii", *args, **kwargs):

        self._proc = None
        self._buffer = bytes() #""
        self._timeout = timeout
        self.encoding = encoding

        logger.debug("Create instance of Conserver...")

    @property
    def timeout(self):
        return self._timeout

    def __del__(self):
        logger.debug("Closing conserver library ...")
        if self._proc:
            self.close_console()

    def list_consoles(self, parameters=None):
        parameters = parameters or ""

        res = []
        try:
            cmd = ["console", "-i"]
            if parameters:
                cmd.append(parameters)

            out = subprocess.check_output(cmd).decode("utf-8")

            logger.debug("List consoles:\n%s" % out)
    
            for line in out.splitlines():

                parts = line.split(":")
                try:
                    name = parts[0]
                    alias = parts[10]
                    res.append((name, alias))
                except IndexError:
                    logger.warn("Console name not found in '%s'" % (line))
        except subprocess.CalledProcessError as e:
            logger.warn("Error listing consoles: %s" % e)

        return sorted(res)
                

    def open_console(self, name, parameters=""):

        logger.debug("Opening console %s ..." % name)

        if self._proc is not None and self._proc.poll() is None:
            raise ValueError("Console already opened. Close it first, if you want to open a new console")

        if not parameters:
            parameters = ""

        try:
            cmd = [ "console", name ]
            if parameters:
                cmd.extend(parameters.split(" "))
#                cmd.append(parameters)

            self._proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            assert self._proc is not None

            if self._proc.poll() is not None:
                # here we can invoke communicate() as it won't block (the process stopped)
                out, err = self._proc.communicate() 
                logger.debug("Console:>> %s" % out)
                if err:
                    logger.warn("Console:<2 %s" % err)
                # the binary had problem starting
                logger.warn("Error opening the 'console' command. Output: %s. Error: %s" % (self._proc.stdout, self._proc.stderr))
                self._proc = None
    
        except OSError:
            raise ValueError("Cannot find 'console' binary. Is it installed and in your path?")
        except Exception:
            raise ValueError("Error unexpected when opening console '%s'" % name)

        self._buffer = bytes() #""

        self.t = Thread(target=async_reader, args=(self._proc, threadbuffer))
        self.t.daemon = True
        self.t.start()
        

    def close_console(self):

        logger.debug("Closing current console ...")

        try:
            if self._proc and self._proc.poll() is None:
                try:
                    # we *must* kill the console process in this way (i.e. with a signal), and 
                    # not with self._proc.kill(), otherwise we have a deadlock:
                    # the main thread would block here until the async_reader thread will
                    # not release its lock on self._proc.stdout, but the thread won't release such
                    # lock until the console process is alive, since it may send new bytes to read
                    os.kill(self._proc.pid, signal.SIGKILL) #signal.SIGTERM)
                except Exception as e:
                    logger.warn("Error killing console process: %s" % e)
                    # when closing the process, an exception is raised when the thread is
                    # reading (in blocking mode) from it
                    #pass
                    raise e

        finally:
            self._proc = None


    def write_data(self, data, encoding=None):

        if not self._proc:
            raise ValueError("You must open a console first")        

        if not encoding:
            encoding = self.encoding

        data = data.encode(encoding)

        logger.debug("Console:>> %s" % data)

        self._proc.stdin.write(data)
        self._proc.stdin.flush()

    def read_until(self, terminator=LF, size=None, encoding=None):
        """
        This function reads all characters up to a terminator sequence. 
        """

        if not encoding:
            encoding = self.encoding

        terminator = terminator.encode(encoding)

        try:
            if size is not None:
                size = int(size)
        except ValueError:
            # the passed argument cannot be converted to integer
            size = None

        if not self._proc:
            raise ValueError("You must open a console first")

        num_read = len(self._buffer)
        last_read = time.time()
        while True:
        
            # consume from the thread
            chunk = bytes()

            # size == None : while True
            # size != None : while (num_read < size)
            while (size is None or num_read < size):
                try:    
                    chunk += threadbuffer.popleft()
                    last_read = time.time()
                    num_read += 1
                except IndexError:
                    break

            self._buffer += chunk

            try:
                part, self._buffer = self._buffer.split(terminator, 1)

                if size is not None and len(part) > size:
                    # if the part is greater than the allowed size, put the remaining
                    # bytes on top of the buffer, and keep only size characters of "part"
                    self._buffer = part[size:] + self._buffer
                    part = part[:size]

                res = part + terminator
                return res.decode(encoding)
            except ValueError:
                # the terminator is not in self._buffer
                pass
            

            diff = time.time() - last_read
            if diff > self.timeout:
                break
            time.sleep(0.1)

        # at this point the buffer does not contain the terminator string
        res = self._buffer.decode(encoding)
        self._buffer = bytes() #""
        return res
