#!/usr/bin/env python

from imp import load_source
from os.path import abspath, dirname, join
from sys import platform


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


CURDIR = dirname(abspath(__file__))
VERSION = (0,1,0)
README = open(join(CURDIR, 'README.rst')).read()
CLASSIFIERS = '\n'.join(
    map(' :: '.join, [
        ('Development Status', '3 - Alpha'),
        ('License', 'OSI Approved', 'Apache Software License'),
        ('Operating System', 'OS Independent'),
        ('Programming Language', 'Python', '2.7'),
        ('Programming Language', 'Python', '3.5'),
        ('Topic', 'Software Development', 'Testing'),
    ])
)


setup(
    name='robotframework-conserverlibrary',
    version='.'.join(map(str, VERSION)),
    description='Robot Framework test library for console connection',
    long_description=README,
    author='Francesco Spegni',
    author_email='francesco.spegni@gmail.com',
    url='https://github.com/fspegni/robotframework-conserverlibrary',
    license='Apache License 2.0',
    keywords='robotframework testing testautomation console conserver',
    platforms='any',
    classifiers=CLASSIFIERS.splitlines(),
    package_dir={'': 'src'},
    packages=['ConserverLibrary'],
    install_requires=['robotframework', ],
)

